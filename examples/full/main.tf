module "lambda_layer" {
  source  = "app.terraform.io/fer1035/lambda-layer/aws"
  version = "1.0.0"

  s3_bucket                = "my-bucket"
  s3_key                   = "my-lambda-package.zip"
  layer_name               = "my-layer"
  description              = "My layer"
  license_info             = "MIT"
  skip_destroy             = false
  compatible_runtimes      = ["python3.11"]
  compatible_architectures = ["x86_64"]
}

output "arn" {
  value = module.lambda_layer.arn
}
