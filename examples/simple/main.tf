module "lambda_layer" {
  source = "app.terraform.io/fer1035/lambda-layer/aws"

  s3_bucket  = "my-bucket"
  s3_key     = "my-lambda-package.zip"
  layer_name = "my-layer"
}

output "arn" {
  value = module.lambda_layer.arn
}
