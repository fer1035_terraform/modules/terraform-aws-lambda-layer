# terraform-aws-lambda-layer

A Terraform module to manage AWS Lambda layers.

## Overview

Lambda layers allow you to add external modules or libraries to your runtime. For example, a PyPI Python module that is not part of the built-in libraries can be added to a compatible Python runtime in Lambda by deploying a layer package for it.

## Deployment Steps

The package structure for different runtimes can be found in [this AWS documentation](https://docs.aws.amazon.com/lambda/latest/dg/packaging-layers.html).

- Package the layer code into a ZIP file as described in the documentation.
    - For Python:
        - Create a clean virtual environment.
        - Install all necessary packages using Pip.
        - Copy the contents of the site-packages directory into a **python** folder.
        - ZIP the **python** folder, ensuring it is at the root of the ZIP file.
- Upload the ZIP file to an S3 bucket that allows access to your Terraform environment.
- Specify the S3 bucket, object key, and (optionally) object version with this module.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.5 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 5.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_lambda_layer_version.lambda_layer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_layer_version) | resource |
| [aws_s3_object.layer_sha256](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/s3_object) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_compatible_architectures"></a> [compatible\_architectures](#input\_compatible\_architectures) | A list of compatible architectures for the layer. | `list(string)` | <pre>[<br>  "x86_64"<br>]</pre> | no |
| <a name="input_compatible_runtimes"></a> [compatible\_runtimes](#input\_compatible\_runtimes) | A list of compatible runtimes for the layer. | `list(string)` | <pre>[<br>  "python3.8",<br>  "python3.9",<br>  "python3.10",<br>  "python3.11",<br>  "python3.12"<br>]</pre> | no |
| <a name="input_description"></a> [description](#input\_description) | A description of the layer. | `string` | `null` | no |
| <a name="input_layer_name"></a> [layer\_name](#input\_layer\_name) | The name of the layer. | `string` | n/a | yes |
| <a name="input_license_info"></a> [license\_info](#input\_license\_info) | License information for the layer (ref: https://spdx.org/licenses/). | `string` | `"GPL-2.0-only"` | no |
| <a name="input_s3_bucket"></a> [s3\_bucket](#input\_s3\_bucket) | The name of the S3 bucket where the layer is stored. | `string` | n/a | yes |
| <a name="input_s3_key"></a> [s3\_key](#input\_s3\_key) | The key of the S3 object where the layer is stored. | `string` | n/a | yes |
| <a name="input_s3_object_version"></a> [s3\_object\_version](#input\_s3\_object\_version) | The version of the S3 object where the layer is stored. | `string` | `null` | no |
| <a name="input_skip_destroy"></a> [skip\_destroy](#input\_skip\_destroy) | Whether to allow the layer to be destroyed. | `bool` | `false` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_arn"></a> [arn](#output\_arn) | The full ARN of the Lambda layer, including version. |
