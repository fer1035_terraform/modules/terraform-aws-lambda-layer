output "arn" {
  description = "The full ARN of the Lambda layer, including version."
  value       = aws_lambda_layer_version.lambda_layer.arn
}
