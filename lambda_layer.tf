data "aws_s3_object" "layer_sha256" {
  bucket = var.s3_bucket
  key    = var.s3_key
}

resource "aws_lambda_layer_version" "lambda_layer" {
  layer_name               = var.layer_name
  compatible_architectures = var.compatible_architectures
  compatible_runtimes      = var.compatible_runtimes
  description              = var.description
  s3_bucket                = var.s3_bucket
  s3_key                   = var.s3_key
  skip_destroy             = var.skip_destroy
  source_code_hash         = chomp(data.aws_s3_object.layer_sha256.body)
}
