variable "s3_bucket" {
  description = "The name of the S3 bucket where the layer is stored."
  type        = string
}

variable "s3_key" {
  description = "The key of the S3 object where the layer is stored."
  type        = string
}

variable "s3_object_version" {
  description = "The version of the S3 object where the layer is stored."
  type        = string
  default     = null
}

variable "layer_name" {
  description = "The name of the layer."
  type        = string
}

variable "compatible_architectures" {
  description = "A list of compatible architectures for the layer."
  type        = list(string)
  default     = ["x86_64"]
}

variable "compatible_runtimes" {
  description = "A list of compatible runtimes for the layer."
  type        = list(string)
  default = [
    "python3.8",
    "python3.9",
    "python3.10",
    "python3.11",
    "python3.12"
  ]
}

variable "description" {
  description = "A description of the layer."
  type        = string
  default     = null
}

variable "license_info" {
  description = "License information for the layer (ref: https://spdx.org/licenses/)."
  type        = string
  default     = "GPL-2.0-only"
}

variable "skip_destroy" {
  description = "Whether to allow the layer to be destroyed."
  type        = bool
  default     = false
}
